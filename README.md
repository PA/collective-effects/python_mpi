# python_mpi

A docker image based on ubuntu:22.04, miniconda and conda-forge to provide:
- python 3.11
- openmpi
- parallel HDF5
- mpi4py
- h5py