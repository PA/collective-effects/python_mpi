# Use a specific, stable base image
FROM ubuntu:22.04

# Set metadata
LABEL name="mbtrack2_container"

ENV DEBIAN_FRONTEND=noninteractive

# Create a user with a home directory and grant sudo access
RUN apt-get update && apt-get install -y sudo && \
    useradd -m -s /bin/bash dockeruser && \
    echo "dockeruser ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/dockeruser

# Switch to the created user
USER dockeruser
WORKDIR /home/dockeruser


# Install system dependencies and Python packages
RUN sudo -E apt-get update && sudo -E apt-get install -y \
    git \
    wget \
    gcc g++ gfortran \
    python3.11 python3.11-dev python3.11-venv python3-pip \
    libopenmpi-dev \
    libhdf5-dev libhdf5-mpi-dev && \
    sudo apt-get clean && \
    sudo rm -rf /var/lib/apt/lists/*

# Set environment variables for HDF5 and MPI
ENV CC=/usr/bin/mpicc
ENV HDF5_MPI=ON
ENV HDF5_LIBDIR=/usr/lib/x86_64-linux-gnu/hdf5/openmpi/
ENV HDF5_INCLUDEDIR=/usr/include/hdf5/openmpi/
RUN python3.11 -m pip install --upgrade pip 
RUN python3.11 -m venv /home/dockeruser/venv
RUN /home/dockeruser/venv/bin/pip install --no-cache-dir \
    "numpy<2.0.0" \
    "tqdm" \
    "pandas>=2.0.0" \
    "scipy>=1.7" \
    "matplotlib>=3.5" \
    "seaborn>=0.12" \
    "mpmath>=1.2.1" \
    "ipykernel" \
    "mpi4py" \
    "accelerator-toolbox" \
    "--no-binary=h5py" \
    "h5py>=3.6"
RUN echo "alias python=/home/dockeruser/venv/bin/python3.11" >> /home/dockeruser/.bashrc
RUN echo "alias python3=/home/dockeruser/venv/bin/python3.11" >> /home/dockeruser/.bashrc
# Ensure .bashrc is sourced in non-interactive shells
RUN echo 'if [ -f ~/.bashrc ]; then . ~/.bashrc; fi' >> /home/dockeruser/.bash_profile
RUN echo 'if [ -f ~/.bashrc ]; then . ~/.bashrc; fi' >> /home/dockeruser/.profile

# Default working directory
WORKDIR /home/dockeruser
# Default command
ENTRYPOINT ["/bin/bash", "-ic", "source /home/dockeruser/venv/bin/activate && bash"]
